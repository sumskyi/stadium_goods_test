# frozen_string_literal: true

require 'json'
require 'net/http'

# fetch data from remote APIs
class GetSocialData
  SOURCES = {
    twitter: 'https://takehome.io/twitter',
    facebook: 'https://takehome.io/facebook',
    instagram: 'https://takehome.io/instagram'
  }.freeze

  def self.call
    threads = []
    result = SOURCES.each_with_object({}) do |item, memo|
      source, url = item
      action = -> { memo[source] = get(url) }

      threads << Thread.new { action.call }
    end
    threads.each(&:join)

    result.to_json
  end

  def self.get(url)
    uri = URI(url)
    response = Net::HTTP.get(uri)

    begin
      JSON.parse response
    rescue JSON::ParserError
      { error: response }
    end
  end
end

# what is happening on the social networks.
# Rack entry point
class SocialNews
  def call(_env)
    [
      200,
      { 'Content-Type' => 'application/json; charset=utf-8' },
      [GetSocialData.call]
    ]
  end
end
